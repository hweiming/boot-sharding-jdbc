package com.example.bootshardingjdbc;

import com.example.bootshardingjdbc.entity.User;
import com.example.bootshardingjdbc.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

@SpringBootTest
class BootShardingJdbcApplicationTests {

	@Test
	void contextLoads() {
	}


	@Autowired
	private UserMapper userMapper;

	/**
	 * sex:奇数
	 * age:奇数
	 * ds1.t_user1
	 */
	@Test
	public void test01() {
		User user = new User();
		user.setNickname("zhangsan" + new Random().nextInt());
		user.setPassword("123456");
		user.setAge(17);
		user.setSex(1);
		user.setBirthday("1997-12-03");
		userMapper.addUser(user);
	}

	/**
	 * sex:奇数
	 * age:偶数
	 * ds1.t_user0
	 */
	@Test
	public void test02() {
		User user = new User();
		user.setNickname("zhangsan" + new Random().nextInt());
		user.setPassword("123456");
		user.setAge(18);
		user.setSex(1);
		user.setBirthday("1997-12-03");
		userMapper.addUser(user);
	}

	/**
	 * sex:偶数
	 * age:奇数
	 * ds0.t_user1
	 */
	@Test
	public void test03() {
		User user = new User();
		user.setNickname("zhangsan" + new Random().nextInt());
		user.setPassword("123456");
		user.setAge(17);
		user.setSex(2);
		user.setBirthday("1997-12-03");
		userMapper.addUser(user);
	}

	/**
	 * sex:偶数
	 * age:偶数
	 * ds0.t_user0
	 */
	@Test
	public void test04() {
		User user = new User();
		user.setNickname("zhangsan" + new Random().nextInt());
		user.setPassword("123456");
		user.setAge(18);
		user.setSex(2);
		user.setBirthday("1997-12-03");
		userMapper.addUser(user);
	}

	/**
	 * 功能描述:子查询类型
	 * @Auther: Solming
	 * @Date:  2022/2/18
	 * @param: []
	 * @return: void
	 **/
	@Test
	public void testSupportSQL1(){
		Long count2  = userMapper.supportCount(1);
		System.out.println("执行成功");
		Long count1 = userMapper.errorCount1();
		System.out.println("执行成功");
	}

	/**
	 * 功能描述: GROUP By 类型,主要时 having 不被支持。
	 * @Auther: Solming
	 * @Date:  2022/2/18
	 * @param: []
	 * @return: void
	 **/
	@Test
	public void testSupportSQL2(){

		userMapper.groupBy1();
		System.out.println("执行成功");
		userMapper.errorGroupBy1();
		System.out.println("执行成功");
		/*userMapper.errorGroupBy2();
		System.out.println("执行成功");

		userMapper.errorGroupBy3();
		System.out.println("执行成功");*/

	}

	/**
	 * 功能描述: distinct，和聚合函数一起不被支持。
	 * @Auther: Solming
	 * @Date:  2022/2/18
	 * @param: []
	 * @return: void
	 **/
	@Test
	public void distinctTest(){

		userMapper.distinctSql1();
		System.out.println("执行成功");

		userMapper.distinctSql2();
		System.out.println("执行成功");
		List<User> users = userMapper.errorDistinctSql2();
		System.out.println("执行成功");
	}



}
