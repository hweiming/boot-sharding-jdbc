package com.example.bootshardingjdbc;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.example.**.mapper"})
public class BootShardingJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootShardingJdbcApplication.class, args);
	}

}
