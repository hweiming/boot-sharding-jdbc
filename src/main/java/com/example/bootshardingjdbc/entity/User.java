package com.example.bootshardingjdbc.entity;

import lombok.Data;

@Data
public class User {
    private Long id;

    private String nickname;

    private String password;

    private Integer sex;

    private String birthday;

    private Integer age;
}