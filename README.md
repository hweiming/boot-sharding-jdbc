# boot-sharding-jdbc

#### 介绍
使用 sharding-jdbc 进行简单的配置读写分离和分库分表的样例。
整理关于使用 SQL 过程中可能存在的坑。

#### 注意

注意修改配置文件的 mysql 链接，示例已关闭不可用。

#### 搭建读写分离的环境

https://juejin.cn/post/6978474131545128974

#### shading-jdbc 存在的坑

1、更新分片字段。ShardingSphereException: Can not update sharding key

2、4.1.1版本between 关键字之前不支持表达式，在5.0.0-alpha提供支持

3、子查询需要包含所有表带的分片分库规则的字段。

常规SQL 就不需要包含全部分片的规则字段，若是没有出现就会变成 【全路由模式】。

4、若是分片规则如求余数的方式，作为 where 后的条件也需要为数值型，不能以 String 入参，或是SQL 上直接赋予为 ''，需要去掉引号。

5、不支持在 where 条件 对规则字段上进行运算。

 
6、不支持包含冗余括号的SQL

7、不支持HAVING

8、不支持OR，UNION 和 UNION ALL

9、不支持特殊INSERT  （insert into tb select * from ）

10、每条INSERT语句只能插入一条数据，不支持VALUES后有多行数据的语句

11、不支持DISTINCT聚合 sum(distinct tagCloumn)

12、不支持dual虚拟表 

13、不支持SELECT LAST_INSERT_ID()

14、不支持CASE WHEN



#### 过程SQL 
########## 读写分离
CREATE TABLE `t_user` (
  `id` bigint(20) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


########## 分库分表
CREATE TABLE `t_user0` (
  `id` bigint(20) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `t_user1` (
  `id` bigint(20) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


